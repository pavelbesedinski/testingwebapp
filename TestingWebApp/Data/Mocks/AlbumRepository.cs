﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using TestingWebApp.Data.Interfaces;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Mocks
{
    /// <summary>
    /// Выполняет работу по получению данных об альбомах с внешнего API
    /// </summary>
    public class AlbumRepository : IAlbum
    {
        private static readonly IUser _userList = new UserRepository();
        public IEnumerable<Album> GetAllAlbums()
        {
            var client = new RestClient($"{URL.JSONPLACEHOLDER}/albums");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                var content = JsonConvert.DeserializeObject<JToken>(response.Content);
                var albums = content.
                    Select(album => new Album()
                    {
                        Id = (int)album["id"],
                        Title = (string)album["title"],
                        User = _userList.GetUserById((int)album["userId"])
                    });
                return albums;
            }
            return null;
        }
        public Album GetAlbumById(int id)
        {
            var client = new RestClient($"{URL.JSONPLACEHOLDER}/albums/{id}");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                var content = JsonConvert.DeserializeObject<JToken>(response.Content);
                return new Album()
                {
                    Id = (int)content["id"],
                    Title = (string)content["title"],
                    User = _userList.GetUserById((int)content["userId"])
                };
            }
            return null;
        }
        public IEnumerable<Album> GetAlbumsByUserId(int userId) 
        { 
            var response = GetAllAlbums().Where(album => album.User.Id == userId);
            return response.Count() == 0 ? null : response; 
        }
    }
}
