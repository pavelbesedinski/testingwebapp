﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using TestingWebApp.Data.Interfaces;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Mocks
{
    /// <summary>
    /// Выполняет работу по получению данных о пользователях с внешнего API
    /// </summary>
    public class UserRepository : IUser
    {
        public IEnumerable<User> GetAllUsers()
        {
            var client = new RestClient($"{URL.JSONPLACEHOLDER}/users");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            if(response.IsSuccessful)
            {
                var content = JsonConvert.DeserializeObject<JToken>(response.Content);
                var users = content.
                    Select(user => new User()
                    {
                        Id  = (int)user["id"],
                        Name = (string)user["name"],
                        Email = (string)user["email"],
                        Username = (string)user["username"],
                        Phone = (string)user["phone"],
                        Website = (string)user["website"],
                        Address = new Address()
                        {
                            Street = (string)user["address"]["street"],
                            Suite = (string)user["address"]["suite"],
                            City = (string)user["address"]["city"],
                            Zipcode = (string)user["address"]["zipcode"],
                            Geo = new Geo()
                            {
                                Lat = (string)user["address"]["geo"]["lat"],
                                Lng = (string)user["address"]["geo"]["lng"],
                            }
                        },
                        Company = new Company()
                        {
                            Name = (string)user["company"]["name"],
                            CatchPhrase = (string)user["company"]["catchPhrase"],
                            Bs = (string)user["company"]["bs"]
                        }
                    });
                return users;
            }
            return null;
        }
        public User GetUserById(int id)
        {
            var client = new RestClient($"{URL.JSONPLACEHOLDER}/users/{id}");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                var user = JsonConvert.DeserializeObject<JToken>(response.Content);
                return new User()
                {
                    Id = id,
                    Name = (string)user["name"],
                    Email = (string)user["email"],
                    Username = (string)user["username"],
                    Phone = (string)user["phone"],
                    Website = (string)user["website"],
                    Address = new Address()
                    {
                        Street = (string)user["address"]["street"],
                        Suite = (string)user["address"]["suite"],
                        City = (string)user["address"]["city"],
                        Zipcode = (string)user["address"]["zipcode"],
                        Geo = new Geo()
                        {
                            Lat = (string)user["address"]["geo"]["lat"],
                            Lng = (string)user["address"]["geo"]["lng"],
                        }
                    },
                    Company = new Company()
                    {
                        Name = (string)user["company"]["name"],
                        CatchPhrase = (string)user["company"]["catchPhrase"],
                        Bs = (string)user["company"]["bs"]
                    }
                };
            }
            return null;
        }
    }
}
