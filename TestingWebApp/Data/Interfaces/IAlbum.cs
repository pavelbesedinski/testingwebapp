﻿using System.Collections.Generic;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с альбомами
    /// </summary>
    public interface IAlbum
    {
        /// <summary>
        /// Получить список всех альбомов
        /// </summary>
        /// <returns>Метод возвращает список альбомов</returns>
        public IEnumerable<Album> GetAllAlbums();

        /// <summary>
        /// Получить альбом по id
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Метод возвращает альбом с идентификатором id</returns>
        public Album GetAlbumById(int id);

        /// <summary>
        /// Получить список альбомов определенного пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Метод возвращает список альбомов пользователя с идентификатором id</returns>
        public IEnumerable<Album> GetAlbumsByUserId(int userId);
    }
}
