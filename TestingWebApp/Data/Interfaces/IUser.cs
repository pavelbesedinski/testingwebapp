﻿using System.Collections.Generic;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с пользователями
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Получить список всех пользователей
        /// </summary>
        /// <returns>Метод возвращает список пользователей</returns>
        public IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Получить пользоваля по id
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Метод возвращает пользователя с идентификатором id</returns>
        public User GetUserById(int id);
    }
}
