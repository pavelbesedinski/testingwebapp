﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestingWebApp.Data.Models
{
    /// <summary>
    /// Модель пользователя
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Электронная почта пользователя
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Юзернейм пользователя
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Адрес пользователя
        /// </summary>
        public Address Address { get; set; }
        /// <summary>
        /// Телефон пользователя
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Вебсайт пользователя
        /// </summary>
        public string Website { get; set; }
        /// <summary>
        /// Компания пользователя
        /// </summary>
        public Company Company { get; set; }
    } 
}
