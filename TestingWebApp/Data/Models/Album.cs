﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestingWebApp.Data.Models
{
    /// <summary>
    /// Модель альбома
    /// </summary>
    public class Album
    {
        /// <summary>
        /// Идентификатор альбома
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Заголовок альбома
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual User User { get; set; }

    }
}
