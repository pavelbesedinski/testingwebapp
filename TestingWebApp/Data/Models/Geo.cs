﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestingWebApp.Data.Models
{
    /// <summary>
    /// Модель geo
    /// </summary>
    public class Geo
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}
