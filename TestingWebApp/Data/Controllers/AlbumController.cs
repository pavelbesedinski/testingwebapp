﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestingWebApp.Data.Interfaces;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Controllers
{
    /// <summary>
    /// Контроллер для работы с альбомами
    /// </summary>
    [ApiController]
    public class AlbumController : Controller
    {
        private readonly IAlbum _album;

        public AlbumController(IAlbum album)
        {
            _album = album;
        }

        /// <summary>
        /// Получить список всех альбомов
        /// </summary>
        /// <returns>Метод возвращает список альбомов</returns>
        [HttpGet("/albums")]
        public IEnumerable<Album> GetAllAlbums() => _album.GetAllAlbums();

        /// <summary>
        /// Получить альбом по id
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Метод возвращает альбом с идентификатором id</returns>
        [HttpGet("/albums/{id}")]
        public Album GetAlbumById(int id) => _album.GetAlbumById(id);

        /// <summary>
        /// Получить список альбомов определенного пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Метод возвращает список альбомов пользователя с идентификатором id</returns>
        [HttpGet("/albums/user/{id}")]
        public IEnumerable<Album> GetAlbumByUserId(int id) => _album.GetAlbumsByUserId(id);
    }
}
