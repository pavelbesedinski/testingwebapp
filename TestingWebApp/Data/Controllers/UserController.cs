﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TestingWebApp.Data.Interfaces;
using TestingWebApp.Data.Models;

namespace TestingWebApp.Data.Controllers
{
    /// <summary>
    /// Контроллер для работы с пользователями
    /// </summary>
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUser _user;

        public UserController(IUser user)
        {
            _user = user;
        }

        /// <summary>
        /// Получить список всех пользователей
        /// </summary>
        /// <returns>Метод возвращает список пользователей</returns>
        [HttpGet("/users")]
        public IEnumerable<User> GetUsers() => _user.GetAllUsers();

        /// <summary>
        /// Получить пользоваля по id
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Метод возвращает пользователя с идентификатором id</returns>
        [HttpGet("/users/{id}")]
        public User GetUsersById(int id) => _user.GetUserById(id);
    }
}
