**The list of albums**
![alt text](https://sun9-36.userapi.com/c206716/v206716832/171e91/ukW8DgMSDIE.jpg)
***
**An album with unique id**
![alt text](https://sun9-53.userapi.com/c206716/v206716832/171e7d/Vv_TImJmDQA.jpg)
***
**Albums belonging to the user**
![alt text](https://sun9-48.userapi.com/c857628/v857628250/22a8c9/0oLA6ROnWm0.jpg)
***
**The list of users json**
![alt text](https://sun9-28.userapi.com/c206716/v206716832/171e69/NX2mYHU-zKs.jpg)
***
**The list of users xml**
![alt text](https://sun9-43.userapi.com/c206716/v206716832/171e73/Z7HfiRRPvlw.jpg)
***
**A user with unique id**
![alt text](https://sun9-64.userapi.com/c206716/v206716832/171e5f/g9DBGI6fUk4.jpg)
***
**The list of albums**
![alt text](https://sun9-64.userapi.com/c206716/v206716832/171e5f/g9DBGI6fUk4.jpg)
***
**The list of the controllers**
![alt text](https://sun9-40.userapi.com/c206716/v206716832/171e9b/c8mTmfjilUU.jpg)
